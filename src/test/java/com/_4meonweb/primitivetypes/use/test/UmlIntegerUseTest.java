package com._4meonweb.primitivetypes.use.test;

import com._4meonweb.primitivetypes.UmlInteger.UmlIntegerMaker;
import com._4meonweb.primitivetypes.test.UmlIntegerBond;
import com._4meonweb.primitivetypes.use.UmlIntegerUse.UmlIntegerMakerUse;

import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.BeforeEach;

@EnableAutoWeld
class UmlIntegerUseTest implements UmlIntegerBond {

  private UmlIntegerMaker ntrlMkr;

  /** Unlimited Natural Maker for tested primitive creation. */
  @Inject
  private Instance<UmlIntegerMaker> ntrlMkrFctr;

  /** Weld initialization. */
  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(UmlIntegerMakerUse.class);

  public UmlIntegerUseTest() {
    super();
  }

  @Override
  public UmlIntegerMaker createMaker() {
    return this.ntrlMkr;
  }

  @Override
  public Stream<Long> getBadValues() {
    return Stream.empty();
  }

  @Override
  public Stream<Long> getGoodValues() {
    return Stream.of(Long.MAX_VALUE - 1, -73457L, 0L, 1L, 923847L);
  }

  @BeforeEach
  void getMakerInstance() {
    this.ntrlMkr = this.ntrlMkrFctr.get();
  }

  @Override
  public Stream<Long[]> getPairsOfDiffValues() {
    final Long[] arr1 = { 0L, 1L };
    final Long[] arr2 = { 3426L, Long.MAX_VALUE - 1 };
    final Long[] arr3 = { Long.MAX_VALUE - 1, 0L };
    final Long[] arr4 = { 234L, 9786L };
    final Long[] arr5 = { -234L, 976L };
    return Stream.of(arr1, arr2, arr3, arr4, arr5);
  }
}
