package com._4meonweb.primitivetypes.use.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlBoolean.UmlBooleanMaker;
import com._4meonweb.primitivetypes.test.UmlBooleanMakerBond;
import com._4meonweb.primitivetypes.use.UmlBooleanUse.UmlBooleanMakerUse;

import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** UML Boolean primitive test.
 *
 * @author Maxim Rodyushkin */
@EnableAutoWeld
class UmlBooleanUseTest implements UmlBooleanMakerBond {
  private UmlBooleanMaker blnMkr;

  /** Boolean Maker for tested primitive creation. */
  @Inject
  private Instance<UmlBooleanMaker> blnMkrFctr;

  /** Weld initialization. */
  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(UmlBooleanMakerUse.class);

  /** Default constructor. */
  public UmlBooleanUseTest() {
    super();
  }

  @Override
  public UmlBooleanMaker createMaker() {
    return this.blnMkr;
  }

  /** All Java Boolean values can be accepted as UML Primitive Boolean
   * values. */
  @Override
  public Stream<Boolean> getBadValues() {
    return Stream.empty();
  }

  @Override
  public Stream<Boolean> getGoodValues() {
    return Stream.of(true, false);
  }

  @BeforeEach
  void getMakerInstance() {
    this.blnMkr = this.blnMkrFctr.get();
  }

  @Override
  public Stream<Boolean[]> getPairsOfDiffValues() {
    final Boolean[] arr1 = { true, false };
    final Boolean[] arr2 = { false, true };
    return Stream.of(arr1, arr2);
  }

  /** Tests matching primitives from literals suitable for boolean FALSE value
   * to static FALSE primitive.
   *
   * @param value
   *          the literal value */
  @Test
  void ofThe_False_Ok() {
    assertEquals(UmlBooleanStatic.FALSE, this.blnMkr.ofThe(false),
        "Value <false> does not match static FALSE.");
  }

  /** Tests matching primitives from literals suitable for boolean TRUE value to
   * static TRUE primitive.
   *
   * @param value
   *          the literal value */
  @Test
  void ofThe_True_Ok() {
    assertEquals(UmlBooleanStatic.TRUE, this.blnMkr.ofThe(true),
        "Value <true> does not match static TRUE.");
  }
}
