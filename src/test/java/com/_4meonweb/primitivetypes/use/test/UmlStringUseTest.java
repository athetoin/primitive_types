package com._4meonweb.primitivetypes.use.test;

import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.primitivetypes.test.UmlStringBond;
import com._4meonweb.primitivetypes.use.UmlStringUse.UmlStringMakerUse;

import java.util.stream.Stream;

import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.jboss.weld.junit5.WeldInitiator;
import org.jboss.weld.junit5.WeldSetup;
import org.jboss.weld.junit5.auto.EnableAutoWeld;
import org.junit.jupiter.api.BeforeEach;

/** EMOF String unit test.
 *
 * @author Maxim Rodyushkin */
@EnableAutoWeld
class UmlStringUseTest implements UmlStringBond {

  private UmlStringMaker strMkr;

  /** Boolean Maker for tested primitive creation. */
  @Inject
  private Instance<UmlStringMaker> strMkrFctr;

  /** Weld initialization. */
  @WeldSetup
  public WeldInitiator weld = WeldInitiator.of(UmlStringMakerUse.class);

  /** Default constructor. */
  public UmlStringUseTest() {
    super();
  }

  @Override
  public UmlStringMaker createMaker() {
    return this.strMkr;
  }

  /** All String values can be accepted as UML Primitive String values. */
  @Override
  public Stream<String> getBadValues() {
    return Stream.empty();
  }

  @Override
  public Stream<String> getGoodValues() {
    return Stream.of("gsy", "", "3456gs\r", "   \t908sd  \t\r\n");
  }

  @BeforeEach
  void getMakerInstance() {
    this.strMkr = this.strMkrFctr.get();
  }

  @Override
  public Stream<String[]> getPairsOfDiffValues() {
    final String[] arr1 = { "", "sad" };
    final String[] arr2 = { "fgyj", "" };
    final String[] arr3 = { "sdjst4", " 78fj" };
    return Stream.of(arr1, arr2, arr3);
  }
}
