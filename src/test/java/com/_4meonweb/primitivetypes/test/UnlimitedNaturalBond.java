package com._4meonweb.primitivetypes.test;

import com._4meonweb.primitivetypes.UmlNatural;
import com._4meonweb.primitivetypes.UmlNatural.NaturalMaker;

/** Unit test for Unlimited Natural primitive.
 *
 * @author Maxim Rodyushkin */
public interface UnlimitedNaturalBond
    extends UmlPrimitiveMakerBond<Long, UmlNatural, NaturalMaker> {

}
