package com._4meonweb.primitivetypes.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlStringStatic;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

class UmlStringStaticTest implements UmlPrimitiveBond<String, UmlString> {

  @Override
  public Stream<
      PrimitiveWithParameters<String, UmlString>> getPrimitiveWithParameters() {
    final var sprrMock = mock(UmlPrimitive.class);
    when(sprrMock.getType()).thenReturn(UmlPrimitiveType.STRING);
    when(sprrMock.getValue()).thenReturn(Optional.of("::"));
    when(sprrMock.toString()).thenReturn("sprrMock");

    final var sprrWrongType = mock(UmlPrimitive.class);
    when(sprrWrongType.getType()).thenReturn(UmlPrimitiveType.NATURAL);
    when(sprrWrongType.getValue()).thenReturn(Optional.of("::"));
    when(sprrWrongType.toString()).thenReturn("sprrWrongType");

    final var alptMock = mock(UmlPrimitive.class);
    when(alptMock.getType()).thenReturn(UmlPrimitiveType.STRING);
    when(alptMock.getValue()).thenReturn(Optional.of("alphabet"));
    when(alptMock.toString()).thenReturn("alptMock");

    final Object[][] arr =
        { { UmlStringStatic.SEPARATOR, "::", UmlPrimitiveType.STRING, sprrMock,
            Arrays.asList(sprrWrongType, alptMock) }, };
    return Arrays.stream(arr).map(line -> {
      final var a = new PrimitveParametersUse<String, UmlString>();
      a.setArr(line);
      return a;
    });
  }
}
