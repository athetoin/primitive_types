package com._4meonweb.primitivetypes.test;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;

public interface UmlStringBond
    extends UmlPrimitiveMakerBond<String, UmlString, UmlStringMaker> {

}
