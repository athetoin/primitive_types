package com._4meonweb.primitivetypes.test;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBoolean.UmlBooleanMaker;

public interface UmlBooleanMakerBond
    extends UmlPrimitiveMakerBond<Boolean, UmlBoolean, UmlBooleanMaker> {

}
