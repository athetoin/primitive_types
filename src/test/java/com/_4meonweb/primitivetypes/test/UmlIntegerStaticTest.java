package com._4meonweb.primitivetypes.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlInteger;
import com._4meonweb.primitivetypes.UmlIntegerStatic;
import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

class UmlIntegerStaticTest implements UmlPrimitiveBond<Long, UmlInteger> {

  @Override
  public Stream<
      PrimitiveWithParameters<Long, UmlInteger>> getPrimitiveWithParameters() {
    final var oneMock = mock(UmlPrimitive.class);
    when(oneMock.getType()).thenReturn(UmlPrimitiveType.INTEGER);
    when(oneMock.getValue()).thenReturn(Optional.of(1L));
    when(oneMock.toString()).thenReturn("oneMock");

    final var zeroMock = mock(UmlPrimitive.class);
    when(zeroMock.getType()).thenReturn(UmlPrimitiveType.INTEGER);
    when(zeroMock.getValue()).thenReturn(Optional.of(0L));
    when(zeroMock.toString()).thenReturn("zeroMock");

    final var oneWrongType = mock(UmlPrimitive.class);
    when(oneWrongType.getType()).thenReturn(UmlPrimitiveType.BOOLEAN);
    when(oneWrongType.getValue()).thenReturn(Optional.of(1L));
    when(oneWrongType.toString()).thenReturn("oneWrongType");

    final var zeroWrongType = mock(UmlPrimitive.class);
    when(zeroWrongType.getType()).thenReturn(UmlPrimitiveType.REAL);
    when(zeroWrongType.getValue()).thenReturn(Optional.of(0L));
    when(zeroWrongType.toString()).thenReturn("zeroWrongType");

    final var minusOneMock = mock(UmlPrimitive.class);
    when(minusOneMock.getType()).thenReturn(UmlPrimitiveType.INTEGER);
    when(minusOneMock.getValue()).thenReturn(Optional.of(-1L));
    when(minusOneMock.toString()).thenReturn("minusOneMock");

    final Object[][] arr = {
        { UmlIntegerStatic.ONE, 1L, UmlPrimitiveType.INTEGER, oneMock,
            Arrays.asList(UmlIntegerStatic.ZERO, zeroMock, oneWrongType,
                minusOneMock) },
        { UmlIntegerStatic.ZERO, 0L, UmlPrimitiveType.INTEGER, zeroMock,
            Arrays.asList(UmlIntegerStatic.ONE, oneMock, zeroWrongType,
                minusOneMock) }, };
    return Arrays.stream(arr).map(line -> {
      final var a = new PrimitveParametersUse<Long, UmlInteger>();
      a.setArr(line);
      return a;
    });
  }
}
