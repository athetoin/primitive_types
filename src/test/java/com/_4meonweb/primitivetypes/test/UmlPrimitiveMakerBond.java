package com._4meonweb.primitivetypes.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitive.UmlPrimitiveMaker;

import java.security.InvalidParameterException;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

/** Common UML Primitive interface constraints.
 *
 * @author Maxim */
public interface UmlPrimitiveMakerBond<T, P extends UmlPrimitive<T, P>,
    M extends UmlPrimitiveMaker<T, P>> {
  /** Creates tested primitive factory.
   *
   * @return the primitive factory */
  M createMaker();

  /** Gets invalid native values for UML Primitive.
   *
   * @return the values */
  Stream<T> getBadValues();

  /** Gets good values.
   *
   * @return the good values */
  Stream<T> getGoodValues();

  /** Get pairs of different native values for testing.
   *
   * @return the pairs */
  Stream<T[]> getPairsOfDiffValues();

  /** Tests reusing maker for primitives with different native values.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> ofThe_AnotherValue_DoesNotMatch() {
    return this.getPairsOfDiffValues().map(
        val -> dynamicTest("check <" + val[0] + ", " + val[1] + ">", () -> {
          final M maker = this.createMaker();
          final var prmv = maker.ofThe(val[0]);
          final var anoPrmv = maker.ofThe(val[1]);
          assertNotSame(prmv, anoPrmv,
              "The same maker creates the same instance");
        }));
  }

  /** Tests NULL pointer exception on NULL native value. */
  @Test
  default void ofThe_Null_ThrowNull() {
    final M maker = this.createMaker();
    assertThrows(NullPointerException.class, () -> maker.ofThe(null),
        "No Exception on Java Primitive NULL.");
  }

  /** Tests equality for the same value.
   *
   * <p>For testing include values produced non-static primitives, if it is
   * possible.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> ofThe_SameValue_Equals() {
    return this.getGoodValues()
        .map(val -> dynamicTest("check <" + val + ">", () -> {
          final M maker = this.createMaker();
          final var prmv = maker.ofThe(val);
          final var anoPrmv = maker.ofThe(val);
          assertEquals(prmv, anoPrmv,
              "Primitives based on the same value are not equal.");
        }));
  }

  /** Tests exception on ineligible native value. */
  @TestFactory
  default Stream<DynamicTest> ofTheGetValue_BadValue_ThrowInvalid() {
    return this.getBadValues()
        .map(val -> dynamicTest("check <" + val + ">", () -> {
          final var mkr = this.createMaker();
          assertThrows(InvalidParameterException.class, () -> mkr.ofThe(val),
              "Invalid value accepted without exception.");
        }));
  }

  /** Tests equality of input and output native values.
   *
   * @return the tests */
  @TestFactory
  default Stream<DynamicTest> ofTheGetValue_GoodValue_Match() {
    return this.getGoodValues()
        .map(val -> dynamicTest("check <" + val + ">", () -> {
          final var prmv = this.createMaker().ofThe(val);
          assertNotNull(prmv,
              () -> "Cannot create primitive for good value <" + val + ">");
          assertEquals(Optional.of(val), prmv.getValue(),
              "Input and output native values are not equal.");
        }));
  }
}
