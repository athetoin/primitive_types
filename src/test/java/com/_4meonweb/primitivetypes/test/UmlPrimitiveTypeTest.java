package com._4meonweb.primitivetypes.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlPrimitiveType;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

class UmlPrimitiveTypeTest {
  @Test
  void conformsTo_InvalidToAll_True() {
    assertEquals(true,
        Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> UmlPrimitiveType.INVALID.conformsTo(typ))
            .allMatch(rslt -> rslt.equals(UmlBooleanStatic.TRUE)));
  }

  @Test
  void conformsTo_VoidToInvalid_False() {
    assertEquals(UmlBooleanStatic.FALSE,
        UmlPrimitiveType.VOID.conformsTo(UmlPrimitiveType.INVALID));
  }

  @Test
  void conformsTo_VoidToNotInvalid_True() {
    assertEquals(true,
        Arrays.stream(UmlPrimitiveType.values())
            .filter(typ -> typ != UmlPrimitiveType.INVALID)
            .map(typ -> UmlPrimitiveType.VOID.conformsTo(typ))
            .allMatch(rslt -> rslt.equals(UmlBooleanStatic.TRUE)));
  }
}
