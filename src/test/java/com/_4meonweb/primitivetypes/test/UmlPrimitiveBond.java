package com._4meonweb.primitivetypes.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public interface UmlPrimitiveBond<T, P extends UmlPrimitive<T, P>> {
  interface PrimitiveWithParameters<T0, P0 extends UmlPrimitive<T0, P0>> {
    P0 get0Primitive();

    T0 get1Value();

    UmlPrimitiveType get2Type();

    UmlPrimitive<T0, P0> get3Equal();

    Stream<Object> get4Unequals();
  }

  @TestFactory
  default Stream<DynamicTest> getAsType_KindOf_Self() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> !prms.get2Type().equals(UmlPrimitiveType.INVALID))
        .map(prms -> prms.get0Primitive())
        .flatMap(prmv -> Arrays.stream(UmlPrimitiveType.values())
            .filter(typ -> prmv.isKindOf(typ).getValue().orElse(false))
            .map(typ -> new Object[] { prmv, typ }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(prms[0], ((UmlPrimitive<?, ?>) prms[0])
                .getAsType((UmlPrimitiveType) prms[1]))));
  }

  @TestFactory
  default Stream<DynamicTest> getAsType_UnkindOf_Invalid() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> !prms.get2Type().equals(UmlPrimitiveType.INVALID))
        .map(prms -> prms.get0Primitive())
        .flatMap(prmv -> Arrays.stream(UmlPrimitiveType.values())
            .filter(typ -> !prmv.isKindOf(typ).getValue().orElse(false))
            .map(typ -> new Object[] { prmv, typ }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(UmlPrimitiveType.INVALID,
                ((UmlPrimitive<?, ?>) prms[0])
                    .getAsType((UmlPrimitiveType) prms[1]).getType())));
  }

  default UmlPrimitive<?, ?> getInvalid() {
    final var invdMock = mock(UmlPrimitive.class);
    when(invdMock.getType()).thenReturn(UmlPrimitiveType.INVALID);
    when(invdMock.toString()).thenReturn("Invalid");
    return invdMock;
  }

  Stream<PrimitiveWithParameters<T, P>> getPrimitiveWithParameters();

  @TestFactory
  default Stream<DynamicTest> getType_Expected_Matched() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get2Type() != null)
        .map(prms -> dynamicTest(
            prms.get0Primitive() + " of type " + prms.get2Type(),
            () -> assertEquals(prms.get2Type(),
                prms.get0Primitive().getType())));
  }

  @TestFactory
  default Stream<DynamicTest> getValue_Expected_Matched() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get1Value() != null)
        .map(prms -> dynamicTest(
            prms.get0Primitive() + " with expected value " + prms.get1Value(),
            () -> assertEquals(Optional.of(prms.get1Value()),
                prms.get0Primitive().getValue())));
  }

  @TestFactory
  default Stream<DynamicTest> isEqual_Equal_True() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get3Equal() != null)
        .map(prms -> dynamicTest(
            prms.get0Primitive() + " with " + prms.get3Equal(),
            () -> assertEquals(UmlBooleanStatic.TRUE,
                prms.get0Primitive().isEqual(prms.get3Equal()))));
  }

  @TestFactory
  default Stream<DynamicTest> isEqual_Invalid_Invalid() {
    return this.getPrimitiveWithParameters().map(prms -> prms.get0Primitive())
        .distinct()
        .map(prmv -> dynamicTest(prmv.toString(),
            () -> assertEquals(UmlBooleanStatic.TRUE,
                prmv.isEqual(this.getInvalid()).isInvalid())));
  }

  @TestFactory
  default Stream<DynamicTest> isEqual_Self_True() {
    return this.getPrimitiveWithParameters()
        .filter(prmr -> !prmr.get2Type().equals(UmlPrimitiveType.INVALID))
        .map(prms -> prms.get0Primitive()).distinct()
        .map(prmv -> dynamicTest(prmv.toString(),
            () -> assertEquals(UmlBooleanStatic.TRUE, prmv.isEqual(prmv))));
  }

  @TestFactory
  default Stream<DynamicTest> isEqual_Unequal_False() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get4Unequals() != null)
        .flatMap(prms -> prms.get4Unequals()
            .map(uneql -> new UmlPrimitive<?, ?>[] { prms.get0Primitive(),
                (UmlPrimitive<?, ?>) uneql }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(UmlBooleanStatic.FALSE,
                prms[0].isEqual(prms[1]))));
  }

  @TestFactory
  default Stream<DynamicTest> isInvalid_Valid_False() {
    return this.getPrimitiveWithParameters()
        .filter(prmr -> !prmr.get2Type().equals(UmlPrimitiveType.INVALID))
        .map(prms -> prms.get0Primitive()).distinct()
        .map(prmv -> dynamicTest(prmv.toString(),
            () -> assertEquals(UmlBooleanStatic.FALSE, prmv.isInvalid())));
  }

  @TestFactory
  default Stream<DynamicTest> isKindOf_KindType_True() {
    return this.getPrimitiveWithParameters().map(prms -> prms.get0Primitive())
        .distinct()
        .flatMap(prmv -> Arrays.stream(UmlPrimitiveType.values()).filter(
            typ -> prmv.getType().conformsTo(typ).equals(UmlBooleanStatic.TRUE))
            .map(typ -> new Object[] { prmv, typ }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(UmlBooleanStatic.TRUE,
                ((UmlPrimitive<?, ?>) prms[0])
                    .isKindOf((UmlPrimitiveType) prms[1]))));
  }

  @TestFactory
  default Stream<DynamicTest> isKindOf_SameType_True() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get2Type() != null)
        .map(prms -> dynamicTest(
            prms.get0Primitive() + " with " + prms.get2Type(),
            () -> assertEquals(UmlBooleanStatic.TRUE,
                prms.get0Primitive().isKindOf(prms.get2Type()))));
  }

  @TestFactory
  default Stream<DynamicTest> isKindOf_UnkindType_False() {
    return this.getPrimitiveWithParameters().map(prms -> prms.get0Primitive())
        .distinct()
        .flatMap(prmv -> Arrays.stream(UmlPrimitiveType.values())
            .filter(typ -> prmv.getType().conformsTo(typ)
                .equals(UmlBooleanStatic.FALSE))
            .map(typ -> new Object[] { prmv, typ }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(UmlBooleanStatic.FALSE,
                ((UmlPrimitive<?, ?>) prms[0])
                    .isKindOf((UmlPrimitiveType) prms[1]))));
  }

  @TestFactory
  default Stream<DynamicTest> isNotEqual_Equal_False() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get3Equal() != null)
        .map(prms -> dynamicTest(
            prms.get0Primitive() + " with " + prms.get3Equal(),
            () -> assertEquals(UmlBooleanStatic.FALSE,
                prms.get0Primitive().isNotEqual(prms.get3Equal()))));
  }

  @TestFactory
  default Stream<DynamicTest> isNotEqual_Invalid_Invalid() {
    return this.getPrimitiveWithParameters().map(prms -> prms.get0Primitive())
        .distinct()
        .map(prmv -> dynamicTest(prmv.toString(),
            () -> assertEquals(UmlBooleanStatic.TRUE,
                prmv.isNotEqual(this.getInvalid()).isInvalid())));
  }

  @TestFactory
  default Stream<DynamicTest> isNotEqual_Self_False() {
    return this.getPrimitiveWithParameters()
        .filter(prmr -> !prmr.get2Type().equals(UmlPrimitiveType.INVALID))
        .map(prms -> prms.get0Primitive()).distinct()
        .map(prmv -> dynamicTest(prmv.toString(),
            () -> assertEquals(UmlBooleanStatic.FALSE, prmv.isNotEqual(prmv))));
  }

  @TestFactory
  default Stream<DynamicTest> isNotEqual_Unequal_True() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get4Unequals() != null)
        .flatMap(prms -> prms.get4Unequals()
            .map(uneql -> new UmlPrimitive<?, ?>[] { prms.get0Primitive(),
                (UmlPrimitive<?, ?>) uneql }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(UmlBooleanStatic.TRUE,
                prms[0].isNotEqual(prms[1]))));
  }

  @TestFactory
  default Stream<DynamicTest> isTypeOf_Other_False() {
    return this.getPrimitiveWithParameters().map(prms -> prms.get0Primitive())
        .distinct()
        .flatMap(prmv -> Arrays.stream(UmlPrimitiveType.values())
            .filter(typ -> !prmv.getType().equals(typ))
            .map(typ -> new Object[] { prmv, typ }))
        .map(prms -> dynamicTest(prms[0] + " with " + prms[1],
            () -> assertEquals(UmlBooleanStatic.FALSE,
                ((UmlPrimitive<?, ?>) prms[0])
                    .isTypeOf((UmlPrimitiveType) prms[1]))));
  }

  @TestFactory
  default Stream<DynamicTest> isTypeOf_Same_True() {
    return this.getPrimitiveWithParameters()
        .filter(prms -> prms.get2Type() != null)
        .map(prms -> dynamicTest(
            prms.get0Primitive() + " with " + prms.get2Type(),
            () -> assertEquals(UmlBooleanStatic.TRUE,
                prms.get0Primitive().isTypeOf(prms.get2Type()))));
  }

  @TestFactory
  default Stream<DynamicTest> isUndefined_Defined_False() {
    return this.getPrimitiveWithParameters()
        .filter(prmr -> !prmr.get2Type().equals(UmlPrimitiveType.INVALID))
        .map(prms -> prms.get0Primitive()).distinct()
        .map(prmv -> dynamicTest(prmv.toString(),
            () -> assertEquals(UmlBooleanStatic.FALSE, prmv.isUndefined())));
  }
}
