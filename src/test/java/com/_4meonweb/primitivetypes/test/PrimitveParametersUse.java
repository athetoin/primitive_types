package com._4meonweb.primitivetypes.test;

import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;
import com._4meonweb.primitivetypes.test.UmlPrimitiveBond.PrimitiveWithParameters;

import java.util.List;
import java.util.stream.Stream;

public class PrimitveParametersUse<T0, P0 extends UmlPrimitive<T0, P0>>
    implements PrimitiveWithParameters<T0, P0> {

  private Object[] line;

  public PrimitveParametersUse() {
    super();
  }

  @SuppressWarnings("unchecked")
  @Override
  public P0 get0Primitive() {
    return (P0) this.line[0];
  }

  @SuppressWarnings("unchecked")
  @Override
  public T0 get1Value() {
    return (T0) this.line[1];
  }

  @Override
  public UmlPrimitiveType get2Type() {
    return (UmlPrimitiveType) this.line[2];
  }

  @SuppressWarnings("unchecked")
  @Override
  public UmlPrimitive<T0, P0> get3Equal() {
    return (UmlPrimitive<T0, P0>) this.line[3];
  }

  @Override
  public Stream<Object> get4Unequals() {
    return ((List<?>) this.line[4]).stream().map(prmv -> prmv);
  }

  void setArr(final Object[] line) {
    this.line = line;
  }

}
