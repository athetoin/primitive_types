package com._4meonweb.primitivetypes.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;

import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlIntegerStatic;
import com._4meonweb.primitivetypes.UmlNaturalStatic;
import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;
import com._4meonweb.primitivetypes.UmlRealStatic;
import com._4meonweb.primitivetypes.UmlStringStatic;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

class PrimitiveInvalidTypeTest {
  @TestFactory
  Stream<DynamicTest> getAsType_OnInvalid_Invalid() {
    return this.getInvalids()
        .flatMap(invd -> Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> new Object[] { invd, typ }))
        .map(prms -> dynamicTest(
            prms[0].getClass().getSimpleName() + " with " + prms[1], () -> {
              assertEquals(UmlPrimitiveType.INVALID,
                  ((UmlPrimitive<?, ?>) prms[0])
                      .getAsType((UmlPrimitiveType) prms[1]).getType());
            }));
  }

  private Stream<UmlPrimitive<?, ?>> getInvalids() {
    return Stream.of(UmlNaturalStatic.INVALID, UmlBooleanStatic.INVALID,
        UmlIntegerStatic.INVALID, UmlRealStatic.INVALID,
        UmlStringStatic.INVALID);
  }

  @TestFactory
  Stream<DynamicTest> getType_OnInvalid_InvalidType() {
    return this.getInvalids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertEquals(UmlPrimitiveType.INVALID, invd.getType())));
  }

  @TestFactory
  Stream<DynamicTest> getValue_OnInvalid_Throw() {
    return this.getInvalids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertThrows(UnsupportedOperationException.class,
                () -> invd.getValue())));
  }

  @TestFactory
  Stream<DynamicTest> isEquals_OnInvalid_Invalid() {
    final var mckdPrmv = mock(UmlPrimitive.class);

    return this.getInvalids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(), () -> {
          assertEquals(UmlPrimitiveType.INVALID,
              invd.isEqual(mckdPrmv).getType());
        }));
  }

  @TestFactory
  Stream<DynamicTest> isInvalid_OnInvalid_True() {
    return this.getInvalids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertEquals(UmlBooleanStatic.TRUE, invd.isInvalid())));
  }

  @TestFactory
  Stream<DynamicTest> isKindOf_OnInvalid_Invalid() {
    return this.getInvalids()
        .flatMap(invd -> Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> new Object[] { invd, typ }))
        .map(prms -> dynamicTest(
            prms[0].getClass().getSimpleName() + " with " + prms[1], () -> {
              assertEquals(UmlPrimitiveType.INVALID,
                  ((UmlPrimitive<?, ?>) prms[0])
                      .isKindOf((UmlPrimitiveType) prms[1]).getType());
            }));
  }

  @TestFactory
  Stream<DynamicTest> isNotEquals_OnInvalid_Invalid() {
    final var mckdPrmv = mock(UmlPrimitive.class);

    return this.getInvalids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(), () -> {
          assertEquals(UmlPrimitiveType.INVALID,
              invd.isNotEqual(mckdPrmv).getType());
        }));
  }

  @TestFactory
  Stream<DynamicTest> isTypeOf_OnInvalid_Invalid() {
    return this.getInvalids()
        .flatMap(invd -> Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> new Object[] { invd, typ }))
        .map(prms -> dynamicTest(
            prms[0].getClass().getSimpleName() + " with " + prms[1], () -> {
              assertEquals(UmlPrimitiveType.INVALID,
                  ((UmlPrimitive<?, ?>) prms[0])
                      .isTypeOf((UmlPrimitiveType) prms[1]).getType());
            }));
  }

  @TestFactory
  Stream<DynamicTest> isUndefined_Invalid_True() {
    return this.getInvalids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertEquals(UmlBooleanStatic.TRUE, invd.isUndefined())));
  }
}
