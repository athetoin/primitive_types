package com._4meonweb.primitivetypes.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;
import com._4meonweb.primitivetypes.UmlReal;
import com._4meonweb.primitivetypes.UmlRealStatic;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

class UmlRealStaticTest implements UmlPrimitiveBond<Double, UmlReal> {

  @Override
  public Stream<
      PrimitiveWithParameters<Double, UmlReal>> getPrimitiveWithParameters() {
    final var zeroMock = mock(UmlPrimitive.class);
    when(zeroMock.getType()).thenReturn(UmlPrimitiveType.REAL);
    when(zeroMock.getValue()).thenReturn(Optional.of(0d));
    when(zeroMock.toString()).thenReturn("zeroMock");

    final var zeroWrongType = mock(UmlPrimitive.class);
    when(zeroWrongType.getType()).thenReturn(UmlPrimitiveType.NATURAL);
    when(zeroWrongType.getValue()).thenReturn(Optional.of(0d));
    when(zeroWrongType.toString()).thenReturn("zeroWrongType");

    final var hnddOneMock = mock(UmlPrimitive.class);
    when(hnddOneMock.getType()).thenReturn(UmlPrimitiveType.REAL);
    when(hnddOneMock.getValue()).thenReturn(Optional.of(100.01d));
    when(hnddOneMock.toString()).thenReturn("hnddOneMock");

    final Object[][] arr = { { UmlRealStatic.ZERO, 0d, UmlPrimitiveType.REAL,
        zeroMock, Arrays.asList(zeroWrongType, hnddOneMock) }, };
    return Arrays.stream(arr).map(line -> {
      final var a = new PrimitveParametersUse<Double, UmlReal>();
      a.setArr(line);
      return a;
    });
  }
}
