package com._4meonweb.primitivetypes.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlIntegerStatic;
import com._4meonweb.primitivetypes.UmlNaturalStatic;
import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;
import com._4meonweb.primitivetypes.UmlRealStatic;
import com._4meonweb.primitivetypes.UmlStringStatic;

import java.util.Arrays;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

class PrimitiveVoidTypeTest {
  @TestFactory
  Stream<DynamicTest> getAsType_OnVoid_Self() {
    return this.getVoids()
        .flatMap(invd -> Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> new Object[] { invd, typ }))
        .map(prms -> dynamicTest(
            prms[0].getClass().getSimpleName() + " with " + prms[1], () -> {
              assertEquals(prms[0], ((UmlPrimitive<?, ?>) prms[0])
                  .getAsType((UmlPrimitiveType) prms[1]));
            }));
  }

  @TestFactory
  Stream<DynamicTest> getType_OnVoid_VoidType() {
    return this.getVoids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertEquals(UmlPrimitiveType.VOID, invd.getType())));
  }

  private Stream<UmlPrimitive<?, ?>> getVoids() {
    return Stream.of(UmlNaturalStatic.VOID, UmlBooleanStatic.VOID,
        UmlIntegerStatic.VOID, UmlRealStatic.VOID, UmlStringStatic.VOID);
  }

  @TestFactory
  Stream<DynamicTest> isEquals_InvalidOnVoid_Invalid() {
    final var invd = mock(UmlPrimitive.class);
    when(invd.getType()).thenReturn(UmlPrimitiveType.INVALID);

    return this.getVoids()
        .map(vds -> dynamicTest(vds.getClass().getSimpleName(), () -> {
          assertEquals(UmlPrimitiveType.INVALID, vds.isEqual(invd).getType());
        }));
  }

  @TestFactory
  Stream<DynamicTest> isEquals_NonvoidOnVoid_False() {
    final var mckdVoid = mock(UmlPrimitive.class);
    when(mckdVoid.getType()).thenReturn(UmlPrimitiveType.INTEGER);

    return this.getVoids()
        .map(vod -> dynamicTest(vod.getClass().getSimpleName(), () -> {
          assertEquals(UmlBooleanStatic.FALSE, vod.isEqual(mckdVoid));
        }));
  }

  @TestFactory
  Stream<DynamicTest> isEquals_VoidOnVoid_True() {
    final var mckdVoid = mock(UmlPrimitive.class);
    when(mckdVoid.getType()).thenReturn(UmlPrimitiveType.VOID);

    return this.getVoids()
        .map(vod -> dynamicTest(vod.getClass().getSimpleName(), () -> {
          assertEquals(UmlBooleanStatic.TRUE, vod.isEqual(mckdVoid));
        }));
  }

  @TestFactory
  Stream<DynamicTest> isInvalid_OnVoid_False() {
    return this.getVoids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertEquals(UmlBooleanStatic.FALSE, invd.isInvalid())));
  }

  @TestFactory
  Stream<DynamicTest> isKindOf_OnVoid_Invalid() {
    return this.getVoids()
        .flatMap(invd -> Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> new Object[] { invd, typ }))
        .map(prms -> dynamicTest(
            prms[0].getClass().getSimpleName() + " with " + prms[1], () -> {
              assertEquals(UmlPrimitiveType.INVALID,
                  ((UmlPrimitive<?, ?>) prms[0])
                      .isKindOf((UmlPrimitiveType) prms[1]).getType());
            }));
  }

  @TestFactory
  Stream<DynamicTest> isNotEquals_InvalidOnVoid_Invalid() {
    final var mckdInvd = mock(UmlPrimitive.class);
    when(mckdInvd.getType()).thenReturn(UmlPrimitiveType.INVALID);

    return this.getVoids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(), () -> {
          assertEquals(UmlPrimitiveType.INVALID,
              invd.isNotEqual(mckdInvd).getType());
        }));
  }

  @TestFactory
  Stream<DynamicTest> isNotEquals_NonvoidOnVoid_False() {
    final var mckdInvd = mock(UmlPrimitive.class);
    when(mckdInvd.getType()).thenReturn(UmlPrimitiveType.INTEGER);

    return this.getVoids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(), () -> {
          assertEquals(UmlBooleanStatic.TRUE, invd.isNotEqual(mckdInvd));
        }));
  }

  @TestFactory
  Stream<DynamicTest> isNotEquals_VoidOnVoid_False() {
    final var mckdInvd = mock(UmlPrimitive.class);
    when(mckdInvd.getType()).thenReturn(UmlPrimitiveType.VOID);

    return this.getVoids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(), () -> {
          assertEquals(UmlBooleanStatic.FALSE, invd.isNotEqual(mckdInvd));
        }));
  }

  @TestFactory
  Stream<DynamicTest> isTypeOf_OnVoid_Invalid() {
    return this.getVoids()
        .flatMap(invd -> Arrays.stream(UmlPrimitiveType.values())
            .map(typ -> new Object[] { invd, typ }))
        .map(prms -> dynamicTest(
            prms[0].getClass().getSimpleName() + " with " + prms[1], () -> {
              assertEquals(UmlPrimitiveType.INVALID,
                  ((UmlPrimitive<?, ?>) prms[0])
                      .isTypeOf((UmlPrimitiveType) prms[1]).getType());
            }));
  }

  @TestFactory
  Stream<DynamicTest> isUndefined_Void_True() {
    return this.getVoids()
        .map(invd -> dynamicTest(invd.getClass().getSimpleName(),
            () -> assertEquals(UmlBooleanStatic.TRUE, invd.isUndefined())));
  }
}
