package com._4meonweb.primitivetypes.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlIntegerStatic;
import com._4meonweb.primitivetypes.UmlNatural;
import com._4meonweb.primitivetypes.UmlNaturalStatic;
import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

class UmlNaturalStaticTest implements UmlPrimitiveBond<Long, UmlNatural> {

  @Override
  public Stream<
      PrimitiveWithParameters<Long, UmlNatural>> getPrimitiveWithParameters() {
    final var oneMock = mock(UmlPrimitive.class);
    when(oneMock.getType()).thenReturn(UmlPrimitiveType.NATURAL);
    when(oneMock.getValue()).thenReturn(Optional.of(1L));
    when(oneMock.toString()).thenReturn("oneMock");

    final var zeroMock = mock(UmlPrimitive.class);
    when(zeroMock.getType()).thenReturn(UmlPrimitiveType.NATURAL);
    when(zeroMock.getValue()).thenReturn(Optional.of(0L));
    when(zeroMock.toString()).thenReturn("zeroMock");

    final var unldMock = mock(UmlPrimitive.class);
    when(unldMock.getType()).thenReturn(UmlPrimitiveType.NATURAL);
    when(unldMock.getValue()).thenReturn(Optional.of(Long.MAX_VALUE));
    when(unldMock.toString()).thenReturn("unldMock");

    final var oneWrongType = mock(UmlPrimitive.class);
    when(oneWrongType.getType()).thenReturn(UmlPrimitiveType.BOOLEAN);
    when(oneWrongType.getValue()).thenReturn(Optional.of(1L));
    when(oneWrongType.toString()).thenReturn("oneWrongType");

    final var zeroWrongType = mock(UmlPrimitive.class);
    when(zeroWrongType.getType()).thenReturn(UmlPrimitiveType.REAL);
    when(zeroWrongType.getValue()).thenReturn(Optional.of(0L));
    when(zeroWrongType.toString()).thenReturn("zeroWrongType");

    final var hnddOneMock = mock(UmlPrimitive.class);
    when(hnddOneMock.getType()).thenReturn(UmlPrimitiveType.NATURAL);
    when(hnddOneMock.getValue()).thenReturn(Optional.of(100L));
    when(hnddOneMock.toString()).thenReturn("minusOneMock");

    final Object[][] arr = {
        { UmlNaturalStatic.ONE, 1L, UmlPrimitiveType.NATURAL, oneMock,
            Arrays.asList(UmlNaturalStatic.ZERO, UmlNaturalStatic.UNLIMITED,
                zeroMock, oneWrongType, hnddOneMock) },
        { UmlNaturalStatic.ZERO, 0L, UmlPrimitiveType.NATURAL, zeroMock,
            Arrays.asList(UmlNaturalStatic.ONE, oneMock, zeroWrongType,
                hnddOneMock) },
        { UmlNaturalStatic.UNLIMITED, Long.MAX_VALUE, UmlPrimitiveType.NATURAL,
            unldMock, Arrays.asList(UmlIntegerStatic.ONE, UmlNaturalStatic.ZERO,
                oneMock, zeroWrongType, hnddOneMock) }, };
    return Arrays.stream(arr).map(line -> {
      final var a = new PrimitveParametersUse<Long, UmlNatural>();
      a.setArr(line);
      return a;
    });
  }
}
