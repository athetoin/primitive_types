package com._4meonweb.primitivetypes.test;

import com._4meonweb.primitivetypes.UmlInteger;
import com._4meonweb.primitivetypes.UmlInteger.UmlIntegerMaker;

/** Unit test for Unlimited Natural primitive.
 *
 * @author Maxim Rodyushkin */
public interface UmlIntegerBond
    extends UmlPrimitiveMakerBond<Long, UmlInteger, UmlIntegerMaker> {

}
