package com._4meonweb.primitivetypes.test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlPrimitive;
import com._4meonweb.primitivetypes.UmlPrimitiveType;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Stream;

class UmlBooleanStaticTest implements UmlPrimitiveBond<Boolean, UmlBoolean> {

  @Override
  public Stream<PrimitiveWithParameters<Boolean,
      UmlBoolean>> getPrimitiveWithParameters() {
    final var trueMock = mock(UmlPrimitive.class);
    when(trueMock.getType()).thenReturn(UmlPrimitiveType.BOOLEAN);
    when(trueMock.getValue()).thenReturn(Optional.of(true));
    when(trueMock.toString()).thenReturn("trueMock");

    final var falseMock = mock(UmlPrimitive.class);
    when(falseMock.getType()).thenReturn(UmlPrimitiveType.BOOLEAN);
    when(falseMock.getValue()).thenReturn(Optional.of(false));
    when(falseMock.toString()).thenReturn("falseMock");

    final var trueWrongType = mock(UmlPrimitive.class);
    when(trueWrongType.getType()).thenReturn(UmlPrimitiveType.INTEGER);
    when(trueWrongType.getValue()).thenReturn(Optional.of(true));
    when(trueWrongType.toString()).thenReturn("trueWrongType");

    final var falseWrongType = mock(UmlPrimitive.class);
    when(falseWrongType.getType()).thenReturn(UmlPrimitiveType.REAL);
    when(falseWrongType.getValue()).thenReturn(Optional.of(false));
    when(falseWrongType.toString()).thenReturn("falseWrongType");

    final Object[][] arr = {
        { UmlBooleanStatic.TRUE, true, UmlPrimitiveType.BOOLEAN, trueMock,
            Arrays.asList(UmlBooleanStatic.FALSE, falseMock, trueWrongType) },
        { UmlBooleanStatic.FALSE, false, UmlPrimitiveType.BOOLEAN, falseMock,
            Arrays.asList(UmlBooleanStatic.TRUE, trueMock, falseWrongType) }, };
    return Arrays.stream(arr).map(line -> {
      final var a = new PrimitveParametersUse<Boolean, UmlBoolean>();
      a.setArr(line);
      return a;
    });
  }
}
