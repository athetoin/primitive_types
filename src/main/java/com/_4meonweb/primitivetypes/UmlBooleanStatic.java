package com._4meonweb.primitivetypes;

import java.util.Optional;

/** Static values of UML Boolean primitive data type.
 *
 * @author Maxim Rodyushkin */
public enum UmlBooleanStatic implements UmlBoolean {
  FALSE, INVALID, TRUE, VOID;

  @Override
  public UmlPrimitiveType getType() {
    if (this.equals(INVALID)) {
      return UmlPrimitiveType.INVALID;
    }
    if (this.equals(VOID)) {
      return UmlPrimitiveType.VOID;
    }
    return UmlPrimitiveType.BOOLEAN;
  }

  @Override
  public Optional<Boolean> getValue() {
    if (!this.equals(INVALID) && !this.equals(VOID)) {
      return Optional.of(Boolean.parseBoolean(this.name()));
    }
    return UmlBoolean.super.getValue();
  }
}
