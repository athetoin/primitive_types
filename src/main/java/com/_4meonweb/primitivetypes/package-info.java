/** The PrimitiveTypes package is an independent package that defines a set of
 * reusable PrimitiveTypes that are commonly used in the definition of
 * metamodels.
 *
 * <p>A PrimitiveType defines a predefined DataType, without any substructure. A
 * PrimitiveType may have algebra and operations defined outside of UML.
 *
 * <p>In this implementation added some static values and simple implementation
 * of methods echoed from OCL Standard library.
 *
 * @author Maxim Rodyushkin */
package com._4meonweb.primitivetypes;