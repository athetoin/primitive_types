package com._4meonweb.primitivetypes;

import java.util.Optional;

public enum UmlIntegerStatic implements UmlInteger {
  INVALID, ONE, VOID, ZERO;

  @Override
  public UmlPrimitiveType getType() {
    if (this.equals(INVALID)) {
      return UmlPrimitiveType.INVALID;
    }
    if (this.equals(VOID)) {
      return UmlPrimitiveType.VOID;
    }
    return UmlPrimitiveType.INTEGER;
  }

  @Override
  public Optional<Long> getValue() {
    switch (this) {
      case ONE:
        return Optional.of(1L);
      case ZERO:
        return Optional.of(0L);
      case INVALID:
        return UmlInteger.super.getValue();
      default:
        return Optional.empty();
    }
  }
}
