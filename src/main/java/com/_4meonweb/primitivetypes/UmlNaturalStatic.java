package com._4meonweb.primitivetypes;

import java.util.Optional;

public enum UmlNaturalStatic implements UmlNatural {
  INVALID, ONE, UNLIMITED, VOID, ZERO;

  @Override
  public UmlPrimitiveType getType() {
    if (this.equals(INVALID)) {
      return UmlPrimitiveType.INVALID;
    }
    if (this.equals(VOID)) {
      return UmlPrimitiveType.VOID;
    }
    return UmlPrimitiveType.NATURAL;
  }

  @Override
  public Optional<Long> getValue() {
    switch (this) {
      case ONE:
        return Optional.of(1L);
      case ZERO:
        return Optional.of(0L);
      case UNLIMITED:
        return Optional.of(Long.MAX_VALUE);
      case INVALID:
        return UmlNatural.super.getValue();
      default:
        return Optional.empty();
    }
  }
}
