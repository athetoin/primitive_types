package com._4meonweb.primitivetypes;

/** UML primitive integer.
 *
 * @author Maxim Rodyushkin */
public interface UmlInteger extends UmlPrimitive<Long, UmlInteger> {
  /** Factory of immutable UML primitive integer from Java primitive datatype.
   *
   * @author Maxim Rodyushkin */
  public interface UmlIntegerMaker
      extends UmlPrimitiveMaker<Long, UmlInteger>, UmlInteger {
  }

  @Override
  default UmlInteger getInvalid() {
    return UmlIntegerStatic.INVALID;
  }
}
