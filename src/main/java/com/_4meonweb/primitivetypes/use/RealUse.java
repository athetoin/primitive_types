package com._4meonweb.primitivetypes.use;

import com._4meonweb.primitivetypes.AbstractPrimitive;
import com._4meonweb.primitivetypes.UmlReal;

/** UML Real primitive type implementation.
 *
 * @author Maxim Rodyushkin */
public class RealUse extends AbstractPrimitive<Double, UmlReal>
    implements UmlReal {

  /** Real primitive builder.
   *
   * @author Maxim Rodyushkin */
  public static class RealMakerUse extends AbstractPrimitive<Double, UmlReal>
      implements RealMaker {

    /** Default builder constructor. */
    protected RealMakerUse() {
      super();
    }
  }

  /** Default Real type constructor. */
  protected RealUse() {
    super();
  }
}
