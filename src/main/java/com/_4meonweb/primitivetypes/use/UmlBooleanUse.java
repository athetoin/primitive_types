package com._4meonweb.primitivetypes.use;

import com._4meonweb.primitivetypes.AbstractPrimitive;
import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

/** UML Boolean type immutable implementation.
 *
 * @author Maxim Rodyushkin */
public class UmlBooleanUse extends AbstractPrimitive<Boolean, UmlBoolean>
    implements UmlBoolean {

  /** UML Boolean builder implementation.
   *
   * @author Maxim Rodyushkin */
  public static class UmlBooleanMakerUse extends
      AbstractPrimitiveMaker<Boolean, UmlBoolean> implements UmlBooleanMaker {

    @Override
    protected UmlBoolean getStaticValue(final Boolean value) {
      if (Boolean.TRUE.equals(value)) {
        return UmlBooleanStatic.TRUE;
      } else {
        return UmlBooleanStatic.FALSE;
      }
    }

    @Override
    protected UmlBoolean ofTheNonNull(final Boolean value) {
      final var umlB = new UmlBooleanUse();
      umlB.value = value;
      return umlB;
    }

  }

  /** Default type implementation. */
  protected UmlBooleanUse() {
    super();
  }
}
