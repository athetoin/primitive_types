package com._4meonweb.primitivetypes.use;

import com._4meonweb.primitivetypes.AbstractPrimitive;
import com._4meonweb.primitivetypes.UmlInteger;
import com._4meonweb.primitivetypes.UmlIntegerStatic;

public class UmlIntegerUse extends AbstractPrimitive<Long, UmlInteger>
    implements UmlInteger {

  public static class UmlIntegerMakerUse extends
      AbstractPrimitiveMaker<Long, UmlInteger> implements UmlIntegerMaker {

    @Override
    protected UmlInteger getStaticValue(final Long value) {
      if (value == 1) {
        return UmlIntegerStatic.ONE;
      } else if (value == 0) {
        return UmlIntegerStatic.ZERO;
      } else {
        return null;
      }
    }

    @Override
    protected UmlInteger ofTheNonNull(final Long value) {
      final var umlS = new UmlIntegerUse();
      umlS.value = value;
      return umlS;
    }

  }

  protected UmlIntegerUse() {
    super();
  }
}
