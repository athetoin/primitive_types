package com._4meonweb.primitivetypes.use;

import com._4meonweb.primitivetypes.AbstractPrimitive;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlStringStatic;

/** UML string default implementation.
 *
 * @author Maxim Rodyushkin */
public class UmlStringUse extends AbstractPrimitive<String, UmlString>
    implements UmlString {

  /** Default implementation of builder of immutable UML string.
   *
   * @author Maxim Rodyushkin */
  public static class UmlStringMakerUse extends
      AbstractPrimitiveMaker<String, UmlString> implements UmlStringMaker {

    @Override
    protected UmlString getStaticValue(final String value) {
      if (value.equals("::")) {
        return UmlStringStatic.SEPARATOR;
      } else {
        return null;
      }
    }

    @Override
    protected UmlString ofTheNonNull(final String value) {
      final var prmv = new UmlStringUse();
      prmv.value = value;
      return prmv;
    }
  }

  /** Default constructor. */
  protected UmlStringUse() {
    super();
  }
}
