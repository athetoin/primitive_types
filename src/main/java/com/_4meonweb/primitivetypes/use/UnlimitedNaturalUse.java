package com._4meonweb.primitivetypes.use;

import com._4meonweb.primitivetypes.AbstractPrimitive;
import com._4meonweb.primitivetypes.UmlNaturalStatic;
import com._4meonweb.primitivetypes.UmlNatural;

import java.security.InvalidParameterException;

public class UnlimitedNaturalUse extends
    AbstractPrimitive<Long, UmlNatural> implements UmlNatural {

  public static class NaturalMakerUse extends
      AbstractPrimitiveMaker<Long, UmlNatural> implements NaturalMaker {

    @Override
    protected UmlNatural getStaticValue(final Long value) {
      if (value == Long.MAX_VALUE) {
        return UmlNaturalStatic.UNLIMITED;
      } else if (value == 1) {
        return UmlNaturalStatic.ONE;
      } else if (value == 0) {
        return UmlNaturalStatic.ZERO;
      } else {
        return null;
      }
    }

    @Override
    protected UmlNatural ofTheNonNull(final Long value) {
      if (Long.MAX_VALUE == value) {
        throw new InvalidParameterException(
            "Too big value to create UML Unlimited Natural");
      }
      if (value < 0) {
        throw new InvalidParameterException(
            "UML Unlimited Natural cannot be negative number");
      }
      final var umlS = new UnlimitedNaturalUse();
      umlS.value = value;
      return umlS;
    }

  }

  protected UnlimitedNaturalUse() {
    super();
  }
}
