package com._4meonweb.primitivetypes;

/** An instance of Real is a value in the (infinite) set of real numbers.
 * Typically an implementation will internally represent Real numbers using a
 * floating point standard such as ISO/IEC/IEEE 60559:2011 (whose content is
 * identical to the predecessor IEEE 754 standard).
 *
 * @author Maxim Rodyushkin */
public interface UmlReal extends UmlPrimitive<Double, UmlReal> {
  /** Factory of immutable UML primitive real.
   *
   * @author Maxim Rodyushkin */
  public interface RealMaker extends UmlReal {
  }

  @Override
  default UmlReal getInvalid() {
    return UmlRealStatic.INVALID;
  }
}
