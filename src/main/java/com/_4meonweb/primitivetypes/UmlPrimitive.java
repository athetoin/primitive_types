package com._4meonweb.primitivetypes;

import java.util.Optional;

/** Common UML Primitive functionality.
 *
 * @author Maxim Rodyushkin */
public interface UmlPrimitive<V, P extends UmlPrimitive<V, P>> {
  /** Common functionality of UML primitive builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <P>
   *          the primitive type */
  public interface UmlPrimitiveMaker<V0, P0 extends UmlPrimitive<V0, P0>>
      extends UmlPrimitive<V0, P0> {

    /** Creates UML Primitive from associated native Java type.
     *
     * @param value
     *          the native value
     * @return the primitive */
    P0 ofThe(V0 value);
  }

  @SuppressWarnings("unchecked")
  default P getAsType(final UmlPrimitiveType type) {
    if (UmlPrimitiveType.INVALID.equals(this.getType())) {
      return this.getInvalid();
    }
    if (UmlPrimitiveType.VOID.equals(this.getType())) {
      return (P) this;
    }
    if (this.isKindOf(type).getValue().orElse(false)) {
      return (P) this;
    }
    return this.getInvalid();
  }

  P getInvalid();

  default UmlPrimitiveType getType() {
    return UmlPrimitiveType.INVALID;
  }

  /** Gets native Java value.
   *
   * @return the native value */
  default Optional<V> getValue() {
    if (UmlPrimitiveType.INVALID.equals(this.getType())) {
      throw new UnsupportedOperationException(
          "Invalid does not support getValue");
    }
    return Optional.empty();
  }

  default UmlBoolean isEqual(final UmlPrimitive<?, ?> object2) {
    if (this.getType().equals(UmlPrimitiveType.INVALID)) {
      return UmlBooleanStatic.INVALID;
    }
    if (UmlPrimitiveType.INVALID.equals(object2.getType())) {
      return UmlBooleanStatic.INVALID;
    }
    if (this == object2) {
      return UmlBooleanStatic.TRUE;
    }
    if (this.getType().equals(object2.getType())
        && this.getValue().equals(object2.getValue())) {
      return UmlBooleanStatic.TRUE;
    }
    return UmlBooleanStatic.FALSE;
  }

  default UmlBoolean isInvalid() {
    return UmlPrimitiveType.INVALID.equals(this.getType())
        ? UmlBooleanStatic.TRUE
        : UmlBooleanStatic.FALSE;
  }

  default UmlBoolean isKindOf(final UmlPrimitiveType type) {
    final var typ = this.getType();
    if (typ.equals(UmlPrimitiveType.INVALID)
        || typ.equals(UmlPrimitiveType.VOID)) {
      return UmlBooleanStatic.INVALID;
    }
    return this.getType().conformsTo(type);
  }

  default UmlBoolean isNotEqual(final UmlPrimitive<?, ?> object2) {
    if (this.getType().equals(UmlPrimitiveType.INVALID)) {
      return UmlBooleanStatic.INVALID;
    }
    if (UmlPrimitiveType.INVALID.equals(object2.getType())) {
      return UmlBooleanStatic.INVALID;
    }
    if (this == object2) {
      return UmlBooleanStatic.FALSE;
    }
    if (this.getType().equals(object2.getType())
        && this.getValue().equals(object2.getValue())) {
      return UmlBooleanStatic.FALSE;
    }
    return UmlBooleanStatic.TRUE;
  }

  default UmlBoolean isTypeOf(final UmlPrimitiveType type) {
    final var typ = this.getType();
    if (typ.equals(UmlPrimitiveType.INVALID)
        || typ.equals(UmlPrimitiveType.VOID)) {
      return UmlBooleanStatic.INVALID;
    }
    return this.getType().equals(type) ? UmlBooleanStatic.TRUE
        : UmlBooleanStatic.FALSE;
  }

  default UmlBoolean isUndefined() {
    return UmlPrimitiveType.INVALID.equals(this.getType())
        || UmlPrimitiveType.VOID.equals(this.getType()) ? UmlBooleanStatic.TRUE
            : UmlBooleanStatic.FALSE;
  }
}
