package com._4meonweb.primitivetypes;

/** Immutable UML primitive string.
 *
 * <p>Default toString needs to be overridden for literal value.
 *
 * @author Maxim Rodyushkin */
public interface UmlString extends UmlPrimitive<String, UmlString> {
  /** UML string factory from Java primitive string.
   *
   * @author Maxim Rodyushkin */
  public interface UmlStringMaker
      extends UmlPrimitiveMaker<String, UmlString>, UmlString {
  }

  @Override
  default UmlString getInvalid() {
    return UmlStringStatic.INVALID;
  }
}
