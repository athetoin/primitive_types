package com._4meonweb.primitivetypes;

/** Primitive boolean type.
 *
 * @author Maxim Rodyushkin */
public interface UmlBoolean extends UmlPrimitive<Boolean, UmlBoolean> {

  /** Interface defining UML primitive Boolean from Java primitive boolean and
   * String.
   *
   * @author Maxim Rodyushkin */
  public interface UmlBooleanMaker
      extends UmlPrimitiveMaker<Boolean, UmlBoolean>, UmlBoolean {
  }

  @Override
  default UmlBoolean getInvalid() {
    return UmlBooleanStatic.INVALID;
  }
}
