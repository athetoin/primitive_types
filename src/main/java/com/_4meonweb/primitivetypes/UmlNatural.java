package com._4meonweb.primitivetypes;

/** An instance of UnlimitedNatural is a value in the (infinite) set of natural
 * numbers (0, 1, 2 ...) plus unlimited. The value of unlimited is shown using
 * an asterisk (*). UnlimitedNatural values are typically used to denote the
 * upper bound of a range, such as a multiplicity; unlimited is used whenever
 * the range is specified to have no upper bound.
 *
 * @author Maxim Rodyushkin */
public interface UmlNatural extends UmlPrimitive<Long, UmlNatural> {

  /** UML Unlimited Natural primitive builder.
   *
   * @author Maxim Rodyushkin */
  public interface NaturalMaker
      extends UmlPrimitiveMaker<Long, UmlNatural>, UmlNatural {
  }

  @Override
  default UmlNatural getInvalid() {
    return UmlNaturalStatic.INVALID;
  }
}
