package com._4meonweb.primitivetypes;

public enum UmlPrimitiveType {
  BOOLEAN, INTEGER, INVALID, REAL, STRING, NATURAL, VOID;

  public UmlBoolean conformsTo(final UmlPrimitiveType type) {
    switch (this) {
      case NATURAL:
        return REAL.equals(type) || INTEGER.equals(type)
            || NATURAL.equals(type) ? UmlBooleanStatic.TRUE
                : UmlBooleanStatic.FALSE;
      case INTEGER:
        return REAL.equals(type) || INTEGER.equals(type) ? UmlBooleanStatic.TRUE
            : UmlBooleanStatic.FALSE;
      case INVALID:
        return UmlBooleanStatic.TRUE;
      case VOID:
        return INVALID.equals(type) ? UmlBooleanStatic.FALSE
            : UmlBooleanStatic.TRUE;
      default:
        return this.equals(type) ? UmlBooleanStatic.TRUE
            : UmlBooleanStatic.FALSE;
    }
  }
}
