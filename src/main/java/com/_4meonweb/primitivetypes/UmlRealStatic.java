package com._4meonweb.primitivetypes;

import java.util.Optional;

public enum UmlRealStatic implements UmlReal {
  INVALID, VOID, ZERO;

  @Override
  public UmlPrimitiveType getType() {
    if (this.equals(INVALID)) {
      return UmlPrimitiveType.INVALID;
    }
    if (this.equals(VOID)) {
      return UmlPrimitiveType.VOID;
    }
    return UmlPrimitiveType.REAL;
  }

  @Override
  public Optional<Double> getValue() {
    switch (this) {
      case ZERO:
        return Optional.of(0d);
      case INVALID:
        return UmlReal.super.getValue();
      default:
        return Optional.empty();
    }
  }
}
