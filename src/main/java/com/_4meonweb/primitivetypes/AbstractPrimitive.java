package com._4meonweb.primitivetypes;

import java.util.Objects;
import java.util.Optional;

/** Common UML primitive type implementation.
 *
 * @author Maxim Rodyushkin
 *
 * @param <V>
 *          the value type */
public abstract class AbstractPrimitive<V, P extends UmlPrimitive<V, P>>
    implements UmlPrimitive<V, P> {

  /** Primitive value. */
  protected V value;

  /** Common UML primitive type builder implementation.
   *
   * @author Maxim Rodyushkin
   *
   * @param <P>
   *          the primitive type */
  public abstract static class AbstractPrimitiveMaker<V0,
      P0 extends UmlPrimitive<V0, P0>> extends AbstractPrimitive<V0, P0>
      implements UmlPrimitiveMaker<V0, P0> {

    /** Default constructor. */
    protected AbstractPrimitiveMaker() {
      super();
    }

    /** Gets static value if literal string is matched to predefined value.
     *
     * <p>If no static value for given literal, it returns NULL.
     *
     * @param value
     *          the literal string
     * @return the primitive value */
    protected abstract P0 getStaticValue(V0 value);

    @Override
    public P0 ofThe(final V0 value) {
      Objects.requireNonNull(value,
          "UML Primitive cannot accept NULL native value.");
      return Optional.ofNullable(this.getStaticValue(value))
          .orElseGet(() -> this.ofTheNonNull(value));
    }

    /** Create UML Primitive without NULL check.
     *
     * @param value
     *          native Java value
     * @return the primitive */
    protected abstract P0 ofTheNonNull(V0 value);
  }

  /** Default constructor. */
  protected AbstractPrimitive() {
    super();
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof AbstractPrimitive)) {
      return false;
    }
    @SuppressWarnings("rawtypes")
    final var other = (AbstractPrimitive) obj;
    return Objects.equals(this.value, other.value);
  }

  @Override
  public Optional<V> getValue() {
    return Optional.of(this.value);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.value);
  }

  @Override
  public String toString() {
    return this.value.toString();
  }
}
