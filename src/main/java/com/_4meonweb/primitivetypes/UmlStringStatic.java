package com._4meonweb.primitivetypes;

import java.util.Optional;

public enum UmlStringStatic implements UmlString {
  INVALID, SEPARATOR, VOID;

  @Override
  public UmlPrimitiveType getType() {
    if (this.equals(INVALID)) {
      return UmlPrimitiveType.INVALID;
    }
    if (this.equals(VOID)) {
      return UmlPrimitiveType.VOID;
    }
    return UmlPrimitiveType.STRING;
  }

  @Override
  public Optional<String> getValue() {
    switch (this) {
      case SEPARATOR:
        return Optional.of("::");
      case INVALID:
        return UmlString.super.getValue();
      default:
        return Optional.empty();
    }
  }
}
